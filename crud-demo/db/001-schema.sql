USE testdb;
create table customer (
    id integer auto_increment,
    name varchar(255) not null,
    code varchar(25) not null,
    address varchar(255),
    enabled TINYINT(1) not null default TRUE,
    primary key(id)
);