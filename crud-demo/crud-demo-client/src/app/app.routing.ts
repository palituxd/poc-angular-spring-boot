import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { EditCustomerComponent } from './edit-customer/edit-customer.component';
import { ListCustomerComponent } from './list-customer/list-customer.component';

const routes: Routes = [
    { path: '', component: LoginComponent},
    { path: 'login', component: LoginComponent },    
    { path: 'add-customer', component: AddCustomerComponent},
    { path: 'edit-customer', component: EditCustomerComponent},
    { path: 'list-customer', component: ListCustomerComponent}    
];

export const routing = RouterModule.forRoot(routes);