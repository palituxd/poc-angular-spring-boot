import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from '../model/customer.model';

@Injectable()
export class CustomerService {

  baseUrl: string = 'http://localhost:8080/crud';

  constructor(private http: HttpClient) { }

  getCustomers() {
    return this.http.get<Customer[]>(this.baseUrl + '/customers');
  }

  getCustomer(id: number) {
    return this.http.get<Customer>(this.baseUrl+'/customer/'+id);
  }

  deleteCustomer(id: number) {
    return this.http.delete<Boolean>(this.baseUrl+'/customer/'+id);
  }

  createCustomer(customer: Customer) {
    return this.http.post(this.baseUrl+'/customer', customer);
  }

  updateCustomer(customer: Customer) {
    return this.http.put(this.baseUrl+'/customer', customer);
  }
}