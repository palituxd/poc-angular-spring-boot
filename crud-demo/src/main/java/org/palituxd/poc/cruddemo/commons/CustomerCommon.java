package org.palituxd.poc.cruddemo.commons;

import org.palituxd.poc.cruddemo.entities.Customer;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerCommon implements Customer {
    private Long id;
    private String name;
    private String code;
    private String address;
    private boolean enabled;
}