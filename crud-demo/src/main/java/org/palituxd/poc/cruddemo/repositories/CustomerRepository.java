package org.palituxd.poc.cruddemo.repositories;

import java.util.List;

import org.palituxd.poc.cruddemo.entities.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Long>{

    @Override
    @Query("SELECT c FROM CustomerEntity c")
    //@Query("SELECT c FROM CustomerEntity c WHERE c.enabled IS TRUE")
    public List<CustomerEntity> findAll();

}