package org.palituxd.poc.cruddemo;

import java.util.List;

import org.palituxd.poc.cruddemo.dto.CustomerDto;
import org.palituxd.poc.cruddemo.operations.CrudOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/crud")
@CrossOrigin("http://localhost:4200")
public class CustomerController {

    @Autowired
    private CrudOperation crudOperation;

    @RequestMapping(path = "/customers", method = RequestMethod.GET)
    public List<CustomerDto> getCustomers() {
        return crudOperation.customerFindAll();
    }

    @RequestMapping(path = "/customer/{id}", method = RequestMethod.GET)
    public CustomerDto getCustomer(@PathVariable Long id) {
        return crudOperation.customerFindOne(id);
    }

    @RequestMapping(path = "/customer/{id}", method = RequestMethod.DELETE)
    public boolean deleteCustomer(@PathVariable Long id) {
        return crudOperation.delete(id);
    }

    @RequestMapping(path = "/customer", method = RequestMethod.POST)
    public CustomerDto saveCustomer(@RequestBody CustomerDto customer) {
        return crudOperation.save(customer);
    }

    @RequestMapping(path = "/customer", method = RequestMethod.PUT)
    public CustomerDto updateCustomer(@RequestBody CustomerDto customer) {
        return crudOperation.update(customer);
    }
}