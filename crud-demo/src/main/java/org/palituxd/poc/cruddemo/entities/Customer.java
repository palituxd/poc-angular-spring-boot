package org.palituxd.poc.cruddemo.entities;

public interface Customer {
    Long getId();
    String getName();
    String getCode();
    String getAddress();
    boolean isEnabled();
}