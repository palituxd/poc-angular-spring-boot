package org.palituxd.poc.cruddemo;

/*
 * Created by: patoche
 * on 15 julio 2018 - 05:53 PM
 */

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:4200")
public class CrudServ {

    @RequestMapping(value = "/saludo", method = RequestMethod.GET)
    public String saludo() {
        return "Hola Mundo :D :D ";
    }
}