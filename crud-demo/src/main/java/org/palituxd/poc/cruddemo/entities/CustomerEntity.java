package org.palituxd.poc.cruddemo.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.palituxd.poc.cruddemo.commons.CustomerCommon;

@Entity
@Table(name = "customer")
public class CustomerEntity extends CustomerCommon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Override
    public Long getId() {
        return super.getId();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getAddress() {
        return super.getAddress();
    }

    @Override
    public String getCode() {
        return super.getCode();
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }
}