package org.palituxd.poc.cruddemo.operations;

import java.util.List;

import org.palituxd.poc.cruddemo.dto.CustomerDto;

public interface CrudOperation {

    public List<CustomerDto> customerFindAll();

    public CustomerDto customerFindOne(long id);

    public CustomerDto save(CustomerDto customerCommon);

    public CustomerDto update(CustomerDto customerCommon);

    public boolean delete(long id);
}