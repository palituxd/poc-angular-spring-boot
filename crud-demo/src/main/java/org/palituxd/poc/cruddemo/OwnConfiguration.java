package org.palituxd.poc.cruddemo;

import org.palituxd.poc.cruddemo.operations.CrudManager;
import org.palituxd.poc.cruddemo.operations.CrudOperation;
import org.palituxd.poc.cruddemo.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OwnConfiguration {

    @Autowired
    private CustomerRepository customerRepository;

    @Bean
    public CrudOperation crudOperation() {
        return CrudManager.builder().customerRepository(customerRepository).build();
    }

}