package org.palituxd.poc.cruddemo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerDto {
    private Long id;
    private String name;
    private String code;
    private String address;
    private boolean enabled;
}