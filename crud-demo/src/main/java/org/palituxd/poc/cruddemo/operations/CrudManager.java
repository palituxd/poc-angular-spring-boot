package org.palituxd.poc.cruddemo.operations;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.palituxd.poc.cruddemo.commons.CustomerCommon;
import org.palituxd.poc.cruddemo.dto.CustomerDto;
import org.palituxd.poc.cruddemo.entities.CustomerEntity;
import org.palituxd.poc.cruddemo.repositories.CustomerRepository;
import org.springframework.beans.BeanUtils;

import lombok.Builder;

@Builder
public class CrudManager implements CrudOperation {

    private CustomerRepository customerRepository;

    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    @Override
    public List<CustomerDto> customerFindAll() {
        List<CustomerEntity> list = customerRepository.findAll();
        Type type = new TypeToken<List<CustomerDto>>() {
        }.getType();
        List<CustomerDto> listDto = MODEL_MAPPER.map(list, type);
        return listDto;
    }

    @Override
    public CustomerDto save(CustomerDto customer) {
        CustomerEntity entity = MODEL_MAPPER.map(customer, CustomerEntity.class);
        customerRepository.saveAndFlush(entity);
        return MODEL_MAPPER.map(entity, CustomerDto.class);
    }

    @Override
    public CustomerDto customerFindOne(long id) {
        Optional<CustomerEntity> o = customerRepository.findById(id);
        if (o.isPresent()) {
            return MODEL_MAPPER.map((CustomerCommon) o.get(), CustomerDto.class);
        }
        return null;
    }

    @Override
    public CustomerDto update(CustomerDto customer) {
        Optional<CustomerEntity> o = customerRepository.findById(customer.getId());
        CustomerEntity entity = o.get();
        BeanUtils.copyProperties(customer, entity);        
        customerRepository.saveAndFlush(entity);
        return MODEL_MAPPER.map((CustomerCommon) entity, CustomerDto.class);
    }

    @Override
    public boolean delete(long id) {
        Optional<CustomerEntity> o = customerRepository.findById(id);
        if (o.isPresent()) {
            try {
                customerRepository.delete(o.get());
                return true;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }
}