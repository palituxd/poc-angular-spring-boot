package org.palituxd.poc.cruddemo;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"org.palituxd.poc.cruddemo"})
public class CrudDemoApplication implements CommandLineRunner {

	/*
	 * @Autowired private CustomerRepository customerRepository;
	 */
	public static void main(String[] args) {
		SpringApplication.run(CrudDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
