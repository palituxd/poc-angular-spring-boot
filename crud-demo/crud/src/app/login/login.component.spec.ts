import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationServiceMock } from '../mocks/auth.service.mock';
import { AuthenticationService } from '../service/auth.service';
import { PasswordValidator } from './password-validator';
import { LoginComponent } from './login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatIconModule } from '@angular/material';
import { MatTableModule } from '@angular/material';
import { MatDialogModule } from '@angular/material';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

describe('LoginComponent', () => {
    let comp: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [LoginComponent],
            providers: [
                { provide:FormBuilder }, 
                { provide:PasswordValidator }, 
                { provide:FormGroup }, 
                { provide:FormControl }, 
                { provide:Validators }, 
                { provide:Router }, 
                { provide: AuthenticationService, useClass: AuthenticationServiceMock }
            ],
            imports: [
                BrowserAnimationsModule,
                MatButtonModule,
                MatInputModule,
                MatFormFieldModule,
                MatIconModule,
                MatTableModule,
                MatDialogModule,
                FormsModule,
                ReactiveFormsModule,
                RouterTestingModule
            ]
        }).compileComponents();
    });

    it('should be created LoginComponent', async(() => {
        const fixture = TestBed.createComponent(LoginComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    }));

    it('should render title in a h1 tag', async(() => {
        const fixture = TestBed.createComponent(LoginComponent);
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('h1').textContent).toContain('Login');
        const app = fixture.debugElement.componentInstance;
        let loginButton: HTMLElement;
        loginButton = fixture.debugElement.nativeElement.querySelectorAll('button')[0];
        fixture.whenStable().then(()=>{
            //loginButton.click();
        })
    }));
});