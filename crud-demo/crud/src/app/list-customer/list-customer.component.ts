import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/customer.model';
import { CustomerService } from '../service/customer.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-list-customer',
  templateUrl: './list-customer.component.html',
  styleUrls: ['./list-customer.component.css']
})
export class ListCustomerComponent implements OnInit {

  customers:Customer[];
  displayedColumns: string[] = ['name', 'code', 'address', 'actionEdit', 'actionDelete'];

  constructor(private router:Router, private customerService:CustomerService) { }

  ngOnInit() {
    this.customers = [];
    this.customerService
    .getCustomers()
    .subscribe(data=>{console.log(data);this.customers = data;});
  }

  addCustomer(): void{
    this.router.navigate(['add-customer']);
  }

  editCustomer(customer:Customer): void{
    localStorage.removeItem("editCustomerId");
    localStorage.setItem("editCustomerId", customer.id.toString());
    this.router.navigate(['edit-customer']);
  }

  deleteCustomer(customer:Customer): void{
    this.customerService.deleteCustomer(customer.id).subscribe(data=>{
      this.customers = this.customers.filter(c=>c!=customer);
    });
  }
}
