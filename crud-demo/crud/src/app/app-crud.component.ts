import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app-crud.component.html',
  styleUrls: ['./app-crud.component.css']
})
export class AppComponent {
  title = 'app';
}